Summarizes the storage usage of a gitlab instance.

# Artifacts

to run use the following parameters:

```
artifacts --url "https://gitlab.company.com" --username "your-username" --token "your-api-token"
```
