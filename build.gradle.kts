import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.30"
}

group = "hu.webvalto"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://kotlin.bintray.com/kotlinx")
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("org.jetbrains.kotlinx:kotlinx-cli:0.3.1")
    implementation("com.google.code.gson:gson:2.8.6")
    implementation("io.github.rybalkinsd:kohttp:0.12.0")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

val runtimeDeps by configurations.creating {
    isCanBeConsumed = true
    isCanBeResolved = true
    extendsFrom(configurations["implementation"])
}

val jar by tasks.getting(Jar::class) {
    manifest {
        attributes["Main-Class"] = "hu.webvalto.gitlab.storage.summarizer.MainKt"
    }

    from(runtimeDeps
            .files
            .map { if (it.isDirectory) it else zipTree(it) })
}
