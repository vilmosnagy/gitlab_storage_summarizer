package hu.webvalto.gitlab.storage.summarizer.pojos.gitlab

import com.google.gson.annotations.SerializedName

data class User (

	@SerializedName("id") val id : Int,
	@SerializedName("name") val name : String,
	@SerializedName("username") val username : String,
	@SerializedName("state") val state : String,
	@SerializedName("avatar_url") val avatar_url : String,
	@SerializedName("web_url") val web_url : String,
	@SerializedName("created_at") val created_at : String,
	@SerializedName("bio") val bio : String,
	@SerializedName("bio_html") val bio_html : String,
	@SerializedName("location") val location : String,
	@SerializedName("public_email") val public_email : String,
	@SerializedName("skype") val skype : String,
	@SerializedName("linkedin") val linkedin : String,
	@SerializedName("twitter") val twitter : String,
	@SerializedName("website_url") val website_url : String,
	@SerializedName("organization") val organization : String,
	@SerializedName("job_title") val job_title : String,
	@SerializedName("work_information") val work_information : String
)
