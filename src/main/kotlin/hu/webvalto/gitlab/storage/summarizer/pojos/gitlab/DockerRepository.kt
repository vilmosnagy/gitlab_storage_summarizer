package hu.webvalto.gitlab.storage.summarizer.pojos.gitlab

import com.google.gson.annotations.SerializedName

data class DockerRepository (

    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("path") val path : String,
    @SerializedName("project_id") val project_id : Int,
    @SerializedName("location") val location : String,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("cleanup_policy_started_at") val cleanup_policy_started_at : String
) {
    lateinit var project: Project
}
