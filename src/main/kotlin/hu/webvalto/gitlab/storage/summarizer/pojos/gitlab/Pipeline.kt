package hu.webvalto.gitlab.storage.summarizer.pojos.gitlab

import com.google.gson.annotations.SerializedName

data class Pipeline (

	@SerializedName("id") val id : Int,
	@SerializedName("sha") val sha : String,
	@SerializedName("ref") val ref : String,
	@SerializedName("status") val status : String,
	@SerializedName("created_at") val created_at : String,
	@SerializedName("updated_at") val updated_at : String,
	@SerializedName("web_url") val web_url : String
)
