package hu.webvalto.gitlab.storage.summarizer.pojos.gitlab

import com.google.gson.annotations.SerializedName

data class Job (

    @SerializedName("id") val id : Int,
    @SerializedName("status") val status : String,
    @SerializedName("stage") val stage : String,
    @SerializedName("name") val name : String,
    @SerializedName("ref") val ref : String,
    @SerializedName("tag") val tag : Boolean,
    @SerializedName("coverage") val coverage : String,
    @SerializedName("allow_failure") val allow_failure : Boolean,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("started_at") val started_at : String,
    @SerializedName("finished_at") val finished_at : String,
    @SerializedName("duration") val duration : String,
    @SerializedName("user") val user : User,
    @SerializedName("commit") val commit : Commit,
    @SerializedName("pipeline") val pipeline : Pipeline,
    @SerializedName("web_url") val web_url : String,
    @SerializedName("artifacts") val artifacts : List<Artifact?>?,
    @SerializedName("runner") val runner : Any,
    @SerializedName("artifacts_expire_at") val artifacts_expire_at : String
)
