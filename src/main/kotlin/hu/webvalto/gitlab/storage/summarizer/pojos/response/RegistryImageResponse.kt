package hu.webvalto.gitlab.storage.summarizer.pojos.response

import java.math.BigDecimal

data class RegistryImageResponse(
    val projectWithNamespace: String,
    val sizeInBytes: BigDecimal,
    val humanizedSize: String,
    val imageCount: Int
)
