package hu.webvalto.gitlab.storage.summarizer.pojos.gitlab

import com.google.gson.annotations.SerializedName

data class ContainerExpirationPolicy (

	@SerializedName("cadence") val cadence : String,
	@SerializedName("enabled") val enabled : Boolean,
	@SerializedName("keep_n") val keep_n : Int,
	@SerializedName("older_than") val older_than : String,
	@SerializedName("name_regex") val name_regex : String,
	@SerializedName("name_regex_keep") val name_regex_keep : String,
	@SerializedName("next_run_at") val next_run_at : String
)
