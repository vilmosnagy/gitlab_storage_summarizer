package hu.webvalto.gitlab.storage.summarizer.pojos.gitlab

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class DockerImage (
    @SerializedName("name") val name : String,
    @SerializedName("total_size") val totalSize : BigDecimal,
    @SerializedName("path") val path : String
) {
    lateinit var repositoryTag: DockerRepositoryTag
}
