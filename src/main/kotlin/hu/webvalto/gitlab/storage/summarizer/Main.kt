package hu.webvalto.gitlab.storage.summarizer

import hu.webvalto.gitlab.storage.summarizer.commands.Artifacts
import hu.webvalto.gitlab.storage.summarizer.commands.BaseGitlabSubcommand
import hu.webvalto.gitlab.storage.summarizer.commands.Registry
import kotlinx.cli.*
import java.util.logging.Level
import java.util.logging.Logger

@ExperimentalCli
fun main(args: Array<String>) {
    Logger.getGlobal().level = Level.ALL
    val parser = ArgParser("Gitlab Storage Summarizer")
    parser.subcommands(Artifacts())
    parser.subcommands(Registry())
    parser.parse(args)
}
