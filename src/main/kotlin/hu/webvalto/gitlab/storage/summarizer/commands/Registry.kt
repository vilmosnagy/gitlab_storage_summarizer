package hu.webvalto.gitlab.storage.summarizer.commands

import com.google.gson.reflect.TypeToken
import hu.webvalto.gitlab.storage.summarizer.formatAsFileSize
import hu.webvalto.gitlab.storage.summarizer.pojos.*
import hu.webvalto.gitlab.storage.summarizer.pojos.gitlab.DockerImage
import hu.webvalto.gitlab.storage.summarizer.pojos.gitlab.DockerRepository
import hu.webvalto.gitlab.storage.summarizer.pojos.gitlab.DockerRepositoryTag
import hu.webvalto.gitlab.storage.summarizer.pojos.gitlab.Project
import hu.webvalto.gitlab.storage.summarizer.pojos.response.RegistryImageResponse
import kotlinx.cli.ExperimentalCli
import java.math.BigDecimal
import java.util.concurrent.atomic.AtomicInteger
import java.util.logging.Logger
import java.util.stream.Collectors
import java.util.stream.Stream

@ExperimentalCli
class Registry : BaseGitlabSubcommand("registry", "Summarize the storage used by the docker registry on your gitlab instance.") {

    private val cntr = AtomicInteger(0)

    companion object {
        private val logger = Logger.getLogger(Artifacts::class.qualifiedName)
    }

    override fun execute() {
        val projects = getAllProjects()
        val dockerImages = projects
            .parallelStream()
            .peek {
                logger.info { "Projects fetched ${cntr.incrementAndGet()} / ${projects.size}" }
            }
            .flatMap(this@Registry::getDockerImagesForProject)
            .collect(Collectors.toSet())
        val dockerImagesBySize: Map<String, BigDecimal> = dockerImages
            .groupingBy { it.repositoryTag.repository.project.nameWithNamespace }
            .fold(BigDecimal.ZERO) { sum, image -> sum.plus(image.totalSize) }
        val dockerImagesByCount: Map<String, Int> = dockerImages
            .groupingBy { it.repositoryTag.repository.project.nameWithNamespace }
            .eachCount()

        val responseList = dockerImagesBySize
            .map { (projectNameWithNamespace, totalBytes) ->
                RegistryImageResponse(
                    projectNameWithNamespace,
                    totalBytes,
                    totalBytes.toLong().formatAsFileSize,
                    dockerImagesByCount[projectNameWithNamespace] ?: 0
                )
            }
            .sortedByDescending { it.sizeInBytes }
            .toList()

        getOutputWriter().use {
            it.write(gson.toJson(responseList))
        }
    }

    private fun getDockerImagesForProject(project: Project): Stream<DockerImage> {
        return getPagedApi(
            "/api/v4/projects/${project.id}/registry/repositories",
            object : TypeToken<Collection<DockerRepository>>() {}
        )
            .stream()
            .peek { it.project = project }
            .flatMap { repository ->
                getPagedApi(
                    "/api/v4/projects/${project.id}/registry/repositories/${repository.id}/tags",
                    object : TypeToken<Collection<DockerRepositoryTag>>() {}
                )
                    .stream()
                    .peek { it.repository = repository }
            }
            .flatMap { repositoryTag ->
                Stream.of(
                    fetchPage(
                        "/api/v4/projects/${project.id}/registry/repositories/${repositoryTag.repository.id}/tags/${repositoryTag.name}",
                        object : TypeToken<DockerImage>() {}
                    )
                ).peek { it.repositoryTag = repositoryTag }
            }

    }
}
