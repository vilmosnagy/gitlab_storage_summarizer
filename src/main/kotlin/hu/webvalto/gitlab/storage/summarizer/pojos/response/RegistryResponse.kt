package hu.webvalto.gitlab.storage.summarizer.pojos.response

data class RegistryResponse(
    val images: List<RegistryImageResponse>
)
