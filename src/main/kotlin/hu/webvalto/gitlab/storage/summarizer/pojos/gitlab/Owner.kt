package hu.webvalto.gitlab.storage.summarizer.pojos.gitlab

import com.google.gson.annotations.SerializedName

data class Owner (

	@SerializedName("id") val id : Int,
	@SerializedName("name") val name : String,
	@SerializedName("username") val username : String,
	@SerializedName("state") val state : String,
	@SerializedName("avatar_url") val avatar_url : String,
	@SerializedName("web_url") val web_url : String
)
