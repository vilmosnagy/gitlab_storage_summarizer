package hu.webvalto.gitlab.storage.summarizer.commands

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import hu.webvalto.gitlab.storage.summarizer.pojos.gitlab.Project
import io.github.rybalkinsd.kohttp.client.client
import io.github.rybalkinsd.kohttp.dsl.httpGet
import io.github.rybalkinsd.kohttp.ext.url
import kotlinx.cli.ArgType
import kotlinx.cli.ExperimentalCli
import kotlinx.cli.Subcommand
import kotlinx.cli.required
import okhttp3.Response
import java.io.BufferedWriter
import java.io.PrintWriter
import java.io.Writer
import java.net.http.HttpClient
import java.nio.file.Files
import java.nio.file.Paths
import java.util.logging.Logger
import java.util.stream.Collectors
import java.util.stream.IntStream

@ExperimentalCli
abstract class BaseGitlabSubcommand(name: String, actionDescription: String): Subcommand(name, actionDescription) {

    val url by option(ArgType.String, "url", "Gitlab instance URL").required()
    val username by option(ArgType.String, "username", "Username to use on the Gitlab API").required()
    val token by option(ArgType.String, "token", "Token to use on the Gitlab API").required()
    val outputFile by option(ArgType.String, "output", "The output file, by default the stdout")

    companion object {
        val gson: Gson = GsonBuilder()
            .setPrettyPrinting()
            .create()
        private val logger = Logger.getLogger(BaseGitlabSubcommand::class.qualifiedName)
        private val httpClient = client {
            readTimeout = 45_000
            writeTimeout = 45_000
            connectTimeout = 45_000
        }
    }

    protected fun getAllProjects(): Collection<Project> {
        return getPagedApi("api/v4/projects", object : TypeToken<Collection<Project>>() {})
    }

    protected fun <T> getPagedApi(endpoint: String, type: TypeToken<Collection<T>>): Collection<T> {
        var page = 1
        try {
            val retList: MutableList<T> = mutableListOf()
            var returnedElements: List<Collection<T>> = emptyList()
            page=1
            while (page == 1 || returnedElements.last().isNotEmpty()) {
                retList.addAll(returnedElements.flatten())
                returnedElements = fetchPages(endpoint, page, page*2, type)
                page *= 2
            }
            retList.addAll(returnedElements.flatten())
            return retList
        } catch (e: Exception) {
            throw IllegalStateException("Error while fetching $url/$endpoint?page=$page", e)
        }
    }

    private fun <T> fetchPages(endpoint: String, fromPageInclusive: Int, toPageExclusive: Int, type: TypeToken<Collection<T>>): List<Collection<T>> {
        logger.info("Fetching pages for $endpoint :  ${fromPageInclusive}..${toPageExclusive-1}")
        return IntStream
            .range(fromPageInclusive, toPageExclusive)
            .parallel()
            .mapToObj { getApiOnPage(endpoint, it)}
            .map { transformResponse(it, type) }
            .collect(Collectors.toList())
    }

    fun <T> fetchPage(endpoint: String, type: TypeToken<T>): T {
        val response = httpGet(client = httpClient) {
            url("$url/$endpoint?private_token=$token")
        }
        if (response.isSuccessful) {
            val string = response.body()?.string()
            if (string != null) {
                return gson.fromJson(string, type.type)
            }
        }
        TODO()
    }

    private fun <T> transformResponse(response: Response, type: TypeToken<Collection<T>>): Collection<T> {
        if (response.isSuccessful) {
            val string = response.body()?.string()
            if (string != null) {
                return gson.fromJson(string, type.type)
            }
        }
        logger.warning { "Error while fetching ${response.request().url()}" }
        return emptySet()
    }

    private fun getApiOnPage(endpoint: String, page: Int): Response {
        logger.info { "Fetching $url/$endpoint?page=$page" }
        return httpGet(client = httpClient) {
            url("$url/$endpoint?private_token=$token&page=$page")
        }
    }

    fun getOutputWriter(): BufferedWriter {
        if (outputFile == null) {
            return BufferedWriter(PrintWriter(System.out))
        } else {
            return Files.newBufferedWriter(Paths.get(outputFile!!))
        }
    }
}
