package hu.webvalto.gitlab.storage.summarizer.pojos.gitlab

import com.google.gson.annotations.SerializedName

data class DockerRepositoryTag (
    @SerializedName("name") val name : String,
    @SerializedName("path") val path : String,
    @SerializedName("location") val location : String
) {
    lateinit var repository: DockerRepository
}
