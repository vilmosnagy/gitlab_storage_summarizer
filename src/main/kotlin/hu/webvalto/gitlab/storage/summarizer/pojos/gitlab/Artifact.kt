package hu.webvalto.gitlab.storage.summarizer.pojos.gitlab

import com.google.gson.annotations.SerializedName
import java.math.BigInteger

data class Artifact(
    @SerializedName("file_type") val fileType: String,
    @SerializedName("size") val size: BigInteger,
    @SerializedName("filename") val filename: String,
    @SerializedName("file_format") val fileFormat: String,
)
