package hu.webvalto.gitlab.storage.summarizer.commands

import com.google.gson.reflect.TypeToken
import hu.webvalto.gitlab.storage.summarizer.pojos.gitlab.Job
import hu.webvalto.gitlab.storage.summarizer.pojos.gitlab.Project
import kotlinx.cli.ExperimentalCli
import java.util.concurrent.atomic.AtomicInteger
import java.util.logging.Logger
import java.util.stream.Collectors
import java.util.stream.Stream

@ExperimentalCli
class Artifacts : BaseGitlabSubcommand("artifacts", "Summarize the storage used by the artifacts on your gitlab instance.") {

    private val cntr = AtomicInteger(0)

    companion object {
        private val logger = Logger.getLogger(Artifacts::class.qualifiedName)
    }

    override fun execute() {
        val projects = getAllProjects()
        val jobs = projects
            .parallelStream()
            .peek {
                logger.info { "Projects fetched ${cntr.incrementAndGet()} / ${projects.size}" }
            }
            .flatMap(this@Artifacts::getJobsForProject)
            .collect(Collectors.toSet())

        val summarizedSize = jobs.flatMap { it.second.artifacts ?: emptyList() }.map { it?.size?.toLong() ?: 0L }.sum()
        val projectsWithLargestArtifacts = jobs
            .toMap()
            .mapValues { job ->
                job.value.artifacts?.map { artifact -> artifact?.size?.toLong() ?: 0L }?.sum() ?: 0L
            }
            .toList()
            .sortedByDescending { it.second }

        println("All the artifacts are $summarizedSize bytes.")
    }

    private fun getJobsForProject(project: Project): Stream<Pair<Project, Job>> {
        return getPagedApi(
            "/api/v4/projects/${project.id}/jobs",
            object : TypeToken<Collection<Job>>() {}
        )
            .stream()
            .map { project to it }
    }
}

