package hu.webvalto.gitlab.storage.summarizer.pojos.gitlab

import com.google.gson.annotations.SerializedName

data class Namespace (

	@SerializedName("id") val id : Int,
	@SerializedName("name") val name : String,
	@SerializedName("path") val path : String,
	@SerializedName("kind") val kind : String,
	@SerializedName("full_path") val full_path : String,
	@SerializedName("parent_id") val parent_id : String,
	@SerializedName("avatar_url") val avatar_url : String,
	@SerializedName("web_url") val web_url : String
)
