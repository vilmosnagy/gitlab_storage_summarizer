package hu.webvalto.gitlab.storage.summarizer.pojos.gitlab

import com.google.gson.annotations.SerializedName

data class Permissions (

	@SerializedName("project_access") val project_access : Any,
	@SerializedName("group_access") val group_access : Any
)
