package hu.webvalto.gitlab.storage.summarizer.pojos.gitlab

import com.google.gson.annotations.SerializedName

data class Links (

	@SerializedName("self") val self : String,
	@SerializedName("issues") val issues : String,
	@SerializedName("merge_requests") val merge_requests : String,
	@SerializedName("repo_branches") val repo_branches : String,
	@SerializedName("labels") val labels : String,
	@SerializedName("events") val events : String,
	@SerializedName("members") val members : String
)
