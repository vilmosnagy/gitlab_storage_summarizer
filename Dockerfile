FROM openjdk:11-jdk-slim AS TEMP_BUILD_IMAGE
WORKDIR /home/gradle/src
COPY . /home/gradle/src
RUN ./gradlew clean assemble

# actual container
FROM openjdk:11
ENV ARTIFACT_NAME=gitlab_storage_lister-1.0-SNAPSHOT.jar
ENV APP_HOME=/usr/app/

WORKDIR $APP_HOME
COPY --from=TEMP_BUILD_IMAGE /home/gradle/src/build/libs/$ARTIFACT_NAME ./app.jar

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app.jar"]
